//
//  MWTVShowsTableViewCell.swift
//  Moovii
//
//  Created by Medhi on 7/19/15.
//  Copyright (c) 2015 MedhiWidjaja. All rights reserved.
//

import UIKit

let tvCollectionViewCellIdentifier: NSString = "TVCollectionCell"

class MWTVShowsTableViewCell: UITableViewCell {

    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var collectionView: MWPosterCollectionView!    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    func setCollectionViewDataSourceDelegate(delegate: protocol<UICollectionViewDelegate,UICollectionViewDataSource>, index: NSInteger) {
        self.collectionView.dataSource = delegate
        self.collectionView.delegate = delegate
        self.collectionView.tag = index
        self.collectionView.reloadData()
    }
    
    func setCollectionViewDataSourceDelegate(delegate: protocol<UICollectionViewDelegate,UICollectionViewDataSource>, indexPath: NSIndexPath) {
        self.collectionView.dataSource = delegate
        self.collectionView.delegate = delegate
        self.collectionView.indexPath = indexPath
        self.collectionView.tag = indexPath.section
        self.collectionView.reloadData()
    }

}
