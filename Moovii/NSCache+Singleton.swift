//
//  NSCache+Singleton.swift
//  Moovii
//
//  Created by Medhi on 7/13/15.
//  Copyright (c) 2015 MedhiWidjaja. All rights reserved.
//

import Foundation

// This extension creates an app-wide instance of NSCache (a singleton) NSCache.sharedInstance
// Usage:
//   To store an object in the cache
//      NSCache.sharedInstance.setObject("String to store", forKey: "key1")
//   To retrieve the object back from the cache:
//      var anObject = NSCache.sharedInstance.objectForKey("key1) as String
//
extension NSCache {
    class var sharedInstance : NSCache {
        struct Static {
            static let instance : NSCache = NSCache()
        }
        return Static.instance
    }
}