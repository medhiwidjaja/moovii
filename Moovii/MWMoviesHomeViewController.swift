//
//  MWMoviesHomeViewController.swift
//  Moovii
//
//  Created by Medhi on 7/12/15.
//  Copyright (c) 2015 MedhiWidjaja. All rights reserved.
//

import UIKit

class MWMoviesHomeViewController: UICollectionViewController {
    private let reuseIdentifier = "MovieHomePosterCell"
    private let sectionInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 50.0, right: 0.0)
    private let processingQueue = NSOperationQueue()
    
    @IBAction func refresh(sender: AnyObject) {
        fetch()
        self.collectionView?.reloadData()
    }
    
    private var results = [TmdbMovieRecord]()
    
    func fetch() {
        MWTmdbClient.sharedClient.get(kMWTmdbMoviePopular, parameters: nil) {
            content, json, error in
            if error != nil {
                println("Error fetching : \(error)")
            }
            if content != nil {
                if let resultArray = content!["results"] as? [AnyObject] {
                    println("Found \(resultArray.count) movies")
                    self.results = resultArray.map { TmdbMovieRecord(dict: $0 as! NSDictionary)}
                }
            }
            self.collectionView?.reloadData()
        }
    }
    
    func posterImageForMovieId(id: String, size: String) {
        
    }
}

extension MWMoviesHomeViewController: UICollectionViewDataSource {
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.results.count ?? 0
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! MWHomePosterCell

        cell.title.text = self.results[indexPath.row].title
        cell.genres.text = self.results[indexPath.row].releaseDate
        
        let baseUrl = NSUserDefaults.standardUserDefaults().stringForKey("image_base_url")!
        if let imagePath = self.results[indexPath.row].posterPath {
            let posterSize = NSUserDefaults.standardUserDefaults().arrayForKey("poster_sizes")![1] as! String
            let posterPath = baseUrl + posterSize + imagePath
            cell.imageView.sd_setImageWithURL(NSURL(string: posterPath), placeholderImage: UIImage(named: "SeasonPosterPlaceholder"))
        }

        return cell
    }
}

extension MWMoviesHomeViewController: UICollectionViewDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetch()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let detailVC = segue.destinationViewController as? MWMovieDetailViewController {
            if let identifier = segue.identifier {
                switch identifier {
                case "Show The Movie Detail":
                    if let indexPath = collectionView?.indexPathForCell(sender as! MWHomePosterCell) {
                        detailVC.tmdbMovieId = results[indexPath.row].id
//                        if let cell = sender as? MWHomePosterCell {
//                            detailVC.imageView.image = cell.imageView?.image
//                        }
                    }
                default: break
                }
            }
        }
    }
}