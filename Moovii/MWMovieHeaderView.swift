//
//  MWMovieHeaderView.swift
//  Moovii
//
//  Created by Medhi on 7/17/15.
//  Copyright (c) 2015 MedhiWidjaja. All rights reserved.
//

import UIKit

class MWMovieHeaderView: UITableViewHeaderFooterView {

    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var movieTitleLabel: UILabel!
    @IBOutlet weak var runningTimeLabel: UILabel!
    @IBOutlet weak var certificationImageView: UIImageView!

    var tmdbMovieJson: JSON?
    
    func setup() {
        if let json = tmdbMovieJson {
            movieTitleLabel.text = json["title"].string
            runningTimeLabel.text = json["runtime"].string
        }
    }
}
