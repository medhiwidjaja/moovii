//
//  MWSeasonListCell.swift
//  Moovii
//
//  Created by Medhi on 7/12/15.
//  Copyright (c) 2015 MedhiWidjaja. All rights reserved.
//

import UIKit

class MWSeasonListCell: UITableViewCell {

    @IBOutlet weak var posterImage: UIImageView!
    @IBOutlet weak var season: UILabel!
    @IBOutlet weak var subtext: UILabel!
//    
//    var imageDownload = NSOperation()
//    
//    // Loads the image at the indicated cell asynchronously 
//    // See the article at http://www.macspotsblog.com/asynchronous-image-loading-for-uitableviewcells/
//    //
//    func loadImageForCell(cell: () -> UITableViewCell?, withContentOfUrl: String?, queue: NSOperationQueue) {
//        
//        imageDownload = NSBlockOperation {
//            var downloadedImage: UIImage?
//
//            if let urlString = withContentOfUrl {
//                if let url = NSURL(string: urlString) {
//                    if let cachedImage: UIImage = NSCache.sharedInstance.objectForKey(urlString) as? UIImage {
//                        downloadedImage = cachedImage
//                        println("Image retrieved from cache")
//                    } else {
//                        if let data = NSData(contentsOfURL: url) {
//                            downloadedImage = UIImage(data: data)
//                            NSCache.sharedInstance.setObject(downloadedImage!, forKey: urlString)
//                            println("Image retrieved from the web")
//                        }
//                    }
//                }
//            }
//            NSOperationQueue.mainQueue().addOperationWithBlock {
//                if let newCell = cell() as? MWSeasonListCell {
//                    newCell.posterImage.image = downloadedImage
//                }
//            }
//        }
//        queue.addOperation(imageDownload)
//        posterImage.image = UIImage(named: "SeasonPosterPlaceholder")
//    }
//    
//    func cancelDownload() {
//        imageDownload.cancel()
//    }
}
