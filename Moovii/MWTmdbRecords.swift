//
//  MWTmdbRecords.swift
//  Moovii
//
//  Created by Medhi on 7/12/15.
//  Copyright (c) 2015 MedhiWidjaja. All rights reserved.
//

import Foundation
import UIKit
import WebImage

struct TmdbMovieRecord {
    let adult: Bool?
    let backdropPath: String?
    var belongsToCollection: String?
    var budget: Double?
    let genreIds: [Int]?
    let id: Int?
    let originalLanguage: String?
    let originalTitle: String?
    let overview: String?
    let releaseDate: String?
    let posterPath: String?
    let popularity: Double?
    let title: String?
    let video: Bool?
    let voteAverage: Double?
    let voteCount: Int?
    let posterSize = "w154"
//    var posterImage: UIImage?
    
    init(dict: NSDictionary) {
        adult = dict["adult"] as? Bool
        backdropPath = dict["backdrop_path"] as? String
        genreIds = dict["genre_ids"] as? [Int]
        id = dict["id"] as? Int
        originalLanguage = dict["original_language"] as? String
        originalTitle = dict["original_title"] as? String
        overview = dict["overview"] as? String
        releaseDate = dict["release_date"] as? String
        posterPath = dict["poster_path"] as? String
        popularity = dict["popularity"] as? Double
        title = dict["title"] as? String
        video = dict["video"] as? Bool
        voteAverage = dict["vote_average"] as? Double
        voteCount = dict["vote_count"] as? Int
//        if let path = posterPath {
//            posterImage = TmdbImage(imageUrl: path, sizeCode: posterSize).poster
//        }
    }
}

struct TmdbTVRecord {
    var backdropPath: String?
    var firstAirDate: String?
    let genreIds: [Int]?
    let id: Int?
    let originalLanguage: String?
    let originalName: String?
    let overview: String?
    let origin_country: [String]?
    let posterPath: String?
    let popularity: Double?
    let name: String?
    let voteAverage: Double?
    let voteCount: Int?
    let posterSize = "w154"
    //var posterImage: UIImage?
    
    init(dict: NSDictionary) {
        backdropPath = dict["backdrop_path"] as? String
        firstAirDate = dict["first_air_date"] as? String
        genreIds = dict["genre_ids"] as? [Int]
        id = dict["id"] as? Int
        originalLanguage = dict["original_language"] as? String
        originalName = dict["original_name"] as? String
        overview = dict["overview"] as? String
        origin_country = dict["origin_country"] as? [String]
        posterPath = dict["poster_path"] as? String
        popularity = dict["popularity"] as? Double
        name = dict["name"] as? String
        voteAverage = dict["vote_average"] as? Double
        voteCount = dict["vote_count"] as? Int
//        if let path = posterPath {
//            posterImage = TmdbImage(imageUrl: path, sizeCode: posterSize).poster
//        }
    }
    
    var backdrops: [TmdbImageRecord]?
    var homepage: String?
    var inProduction: Bool?
    var languages: [String]?
    var lastAirDate: String?
    var seasons: [TmdbSeasonRecord]?
}

struct TmdbSeasonRecord {
    struct Cast {
        let character: String?
        let credit_id: String?
        let id: Int
        let name: String?
        let profile_path: String?
        let order: Int?
    }
    struct Crew {
        let department: String?
        let id: Int
        let name: String?
        let profile_path: String?
        let job: String?
    }
    
    let cast: [Cast]?
    let crew: [Crew]?
    let id: Int
    let seasonNumber: Int
    
    var posters: [TmdbImageRecord]?
    var videos: [TmdbVideoRecord]?
}

struct TmdbImageRecord {
    let aspectRatio: Double?
    let filePath: String?
    let height: Int?
    let width: Int?
    let voteAverage: Double?
    let voteCount: Int?
    let iso_639_1: String?
}

struct TmdbVideoRecord {
    let id: String
    let iso_639_1: String?
    let key: String?
    let name: String?
    let site: String?
    let size: Int?
    let type: String?
}

struct TmdbConfiguration {
    var imageBaseUrl: String?
    var imageSecureBaseUrl: String?
    var backdropSizes: [String]?
    var logoSizes: [String]?
    var posterSizes: [String]?
    var profileSizes: [String]?
    var stillSizes: [String]?
    let changeKeys: [String]?
    
    init(dict: NSDictionary) {
        if let images = dict["images"] as? NSDictionary {
            imageBaseUrl = images["base_url"] as? String
            imageSecureBaseUrl = images["secure_base_url"] as? String
            backdropSizes = images["backdrop_sizes"] as? [String]
            logoSizes = images["logo_sizes"] as? [String]
            posterSizes = images["poster_sizes"] as? [String]
            profileSizes = images["profile_sizes"] as? [String]
            stillSizes = images["still_sizes"] as? [String]
        }
        changeKeys = dict["change_keys"] as? [String]
    }
}

struct TmdbImage {
    let sizeCode: String
    let imageUrl: String
    let baseUrl: String
    let fullUrl: String
    var poster: UIImage?
    
    init (imageUrl: String, sizeCode: String) {
        self.imageUrl = imageUrl
        self.sizeCode = sizeCode
        self.baseUrl = NSUserDefaults.standardUserDefaults().stringForKey("image_base_url")!
        self.fullUrl = baseUrl + sizeCode + imageUrl
        if let url = NSURL(string: self.fullUrl) {
            if let data = NSData(contentsOfURL: url) {
                poster = UIImage(data: data)
            }
        }
    }
}
