//
//  MooviiImageCell.swift
//  Moovii
//
//  Created by Medhi on 7/9/15.
//  Copyright (c) 2015 MedhiWidjaja. All rights reserved.
//

import UIKit

struct CellIndex {
    let section: Int
    let item: Int
}

class MWMoviePosterCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
  
    var cellIndex: CellIndex?
    
}
