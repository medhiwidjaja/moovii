//
//  MWTmdbConstants.swift
//  Moovi
//
//  Created by Medhi on 6/25/15.
//  Copyright (c) 2015 MedhiWidjaja. All rights reserved.
//

import Foundation

// MARK: - API URL
public let kMWTmdbAPISSL     = "https"
public let kMWTmdbAPINoSSL   = "http"
public let kMWTmdbAPIBaseURL = "://api.themoviedb.org/"
public let kMWTmdbAPIVersion = "3/"

// MARK: - Configuration
// Documentation: http://docs.themoviedb.apiary.io/#configuration
public let kMWTmdbConfiguration = "configuration"

// MARK: - Movies
// Documentation: http://docs.themoviedb.apiary.io/#movies
public let kMWTmdbMovie         = "movie/{id}"
public let kMWTmdbMovieAlternativeTitle = "movie/{id}/alternative_titles"
public let kMWTmdbMovieCredits  = "movie/{id}/credits"
public let kMWTmdbMovieImages   = "movie/{id}/images"
public let kMWTmdbMovieKeywords = "movie/{id}/keywords"
public let kMWTmdbMovieReleases = "movie/{id}/releases"
public let kMWTmdbMovieTrailers = "movie/{id}/trailers"
public let kMWTmdbMovieTranslations = "movie/{id}/translations"
public let kMWTmdbMovieSimilar  = "movie/{id}/similar_movies"
public let kMWTmdbMovieReviews  = "movie/{id}/reviews"
public let kMWTmdbMovieLists    = "movie/{id}/lists"
public let kMWTmdbMovieChanges  = "movie/{id}/changes"
public let kMWTmdbMovieLatest   = "movie/latest"
public let kMWTmdbMovieUpcoming = "movie/upcoming"
public let kMWTmdbMovieNowPlaying = "movie/now_playing"
public let kMWTmdbMoviePopular  = "movie/popular"
public let kMWTmdbMovieTopRated = "movie/top_rated"

// MARK: - TV Shows
// Documentation: http://docs.themoviedb.apiary.io/#tv
public let kMWTmdbTV                 = "tv/{id}"
public let kMWTmdbTVAlternativeTitle = "tv/{id}/alternative_titles"
public let kMWTmdbTVChanges          = "tv/{id}/changes"
public let kMWTmdbTVContentRatings   = "tv/{id}/content_ratings"
public let kMWTmdbTVCredits          = "tv/{id}/credits"
public let kMWTmdbTVExternalIds      = "tv/{id}/external_ids"
public let kMWTmdbTVImages           = "tv/{id}/images"
public let kMWTmdbTVKeywords         = "tv/{id}/keywords"
public let kMWTmdbTVRatings          = "tv/{id}/ratings"
public let kMWTmdbTVSimilar          = "tv/{id}/similar"
public let kMWTmdbTVTranslations     = "tv/{id}/translations"
public let kMWTmdbTVVideos           = "tv/{id}/videos"
public let kMWTmdbTVLatest           = "tv/latest"
public let kMWTmdbTVOnTheAir         = "tv/on_the_air"
public let kMWTmdbTVAiringToday      = "tv/airing_today"
public let kMWTmdbTVPopular          = "tv/popular"
public let kMWTmdbTVTopRated         = "tv/top_rated"

// MARK: - TV Seasons
// Documentation: http://docs.themoviedb.apiary.io/#season
public let kMWTmdbTVSeasonNumber        = "tv/{id}/season/{season_number}"
public let kMWTmdbTVSeasonChanges       = "tv/season/{id}/changes"
public let kMWTmdbTVSeasonAccountStates = "tv/{id}/season/{season_number}/account_states"
public let kMWTmdbTVSeasonCredits       = "tv/{id}/season/{season_number}/credits"
public let kMWTmdbTVSeasonExternalIds   = "tv/{id}/season/{season_number}/external_ids"
public let kMWTmdbTVSeasonImages        = "tv/{id}/season/{season_number}/images"
public let kMWTmdbTVSeasonVideos        = "tv/{id}/season/{season_number}/videos"

// MARK: - TV Episodes
// Documentation: http://docs.themoviedb.apiary.io/#episode
public let kMWTmdbTVEpisodeNumber        = "tv/{id}/season/{season_number}/episode/{episode_number}"
public let kMWTmdbTVEpisodeChanges       = "tv/episode/{id}/changes"
public let kMWTmdbTVEpisodeAccountStates = "tv/{id}/season/{season_number}/episode/{episode_number}/account_states"
public let kMWTmdbTVEpisodeCredits       = "tv/{id}/season/{season_number}/episode/{episode_number}/credits"
public let kMWTmdbTVEpisodeExternalIds   = "tv/{id}/season/{season_number}/episode/{episode_number}/external_ids"
public let kMWTmdbTVEpisodeImages        = "tv/{id}/season/{season_number}/episode/{episode_number}/images"
public let kMWTmdbTVEpisodeRating        = "tv/{id}/season/{season_number}/episode/{episode_number}/rating"
public let kMWTmdbTVEpisodeVideos        = "tv/{id}/season/{season_number}/episode/{episode_number}/videos"

// MARK: - People
// Documentation: http://docs.themoviedb.apiary.io/#people
public let kMWTmdbPerson        = "person/{id}"
public let kMWTmdbPersonCredits = "person/{id}/combined_credits"
public let kMWTmdbPersonImages  = "person/{id}/images"
public let kMWTmdbPersonChanges = "person/{id}/changes"
public let kMWTmdbPersonPopular = "person/popular"
public let kMWTmdbPersonLatest  = "person/latest"

// MARK: - Companies
// Documentation: http://docs.themoviedb.apiary.io/#companies
public let kMWTmdbCompany       = "company/{id}"
public let kMWTmdbCompanyMovies = "company/{id}/movies"

// MARK: - Genres
// Documentation: http://docs.themoviedb.apiary.io/#genres
public let kMWTmdbGenreList   = "genre/list"
public let kMWTmdbGenreMovies = "genre/{id}/movies"

// MARK: - Keywords
// Documentation: http://docs.themoviedb.apiary.io/#keywords
public let kMWTmdbKeyword       = "keyword/{id}"
public let kMWTmdbKeywordMovies = "keyword/{id}/movies"

// MARK: - Search
// Documentation: http://docs.themoviedb.apiary.io/#search
public let kMWTmdbSearchMovie   = "search/movie"
public let kMWTmdbSearchPerson  = "search/person"
public let kMWTmdbSearchCompany = "search/company"