//
//  MWMovieHeaderCell.swift
//  Moovii
//
//  Created by Medhi on 7/15/15.
//  Copyright (c) 2015 MedhiWidjaja. All rights reserved.
//

import UIKit

class MWMovieHeaderCell: UITableViewCell {

    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var movieTitleLabel: UILabel!
    @IBOutlet weak var certificationImageView: UIImageView!
    @IBOutlet weak var runningTimeLabel: UILabel!
    @IBOutlet weak var genresLabel: UILabel!
    @IBOutlet weak var ratingsLabel: UILabel!
    @IBOutlet weak var votesLabel: UILabel!
    @IBOutlet weak var certificationLabel: UILabel!

}
