//
//  MWMovieDetailViewController.swift
//  Moovii
//
//  Created by Medhi on 7/10/15.
//  Copyright (c) 2015 MedhiWidjaja. All rights reserved.
//

import UIKit

private let movieHeaderCellReuseIdentifier = "Movie Header"
private let segmentedControlCellReuseIdentifier = "Segmented Controls"
private let movieOverviewCellReuseIdentifier = "MovieOverview"
private let movieCastCellReuseIdentifier = "Movie Cast"
private let castCollectionCellReuseIdentifier = "CastCell"
private let crewCellReuseIdentifier = "CrewCell"

class MWMovieDetailViewController: UITableViewController {

    @IBOutlet weak var imageView: UIImageView!
    
    let bigPosterSize = NSUserDefaults.standardUserDefaults().stringArrayForKey("poster_sizes")![4] as! String
    var tmdbMovieId: Int?
    var tmdbMovieJson = JSON.nullJSON
    var movieCastJson = JSON.nullJSON
    var movieCrewJson = JSON.nullJSON
    var certificationJson = JSON.nullJSON
    var posterPath: String?
    var backdropPath: String?
    var overview: String?
    var movieTitle: String?
    var popularity: Double?
    let baseUrl = NSUserDefaults.standardUserDefaults().stringForKey("image_base_url")!
    let posterSize = NSUserDefaults.standardUserDefaults().stringArrayForKey("poster_sizes")![1] as! String
    let backdropSize = NSUserDefaults.standardUserDefaults().stringArrayForKey("backdrop_sizes")![0] as! String
    var posterImage: UIImage?
    var backdropImageView = UIImageView()
    
    // TODO: combine the 3 requests into one using url
    //  /movie/{id}?api_key=#####&append_to_response=credits,releases
    
    func fetch() {
        if let id = tmdbMovieId {
            MWTmdbClient.sharedClient.get(kMWTmdbMovie, parameters: ["id": id.description, "append_to_response": "releases"]) {
                content, jsonData, error in
                if error != nil {
                    println("Error fetching : \(error)")
                }
                if let json = jsonData {
                    self.tmdbMovieJson = json
                    self.movieTitle = self.tmdbMovieJson["title"].stringValue
                    self.posterPath = self.baseUrl + self.posterSize + json["poster_path"].stringValue
                    self.backdropPath = self.baseUrl + self.backdropSize + json["backdrop_path"].stringValue
                    if json["releases"] != nil {
                        self.certificationJson = json["releases"]["countries"]
                    }
                } else {
                    println("Json is absent")
                }
                self.tableView?.reloadData()
            }
            MWTmdbClient.sharedClient.get(kMWTmdbMovieCredits, parameters: ["id": id.description]) {
                content, jsonData, error in
                if let json = jsonData {
                    self.movieCastJson = json["cast"]
                    self.movieCrewJson = json["crew"]
                }
                self.tableView?.reloadData()
            }
        }
    }
}


// MARK: - Table view delegate
extension MWMovieDetailViewController: UITableViewDelegate {
    override func viewDidLoad() {
        super.viewDidLoad()
        //tableView.registerNib(UINib(nibName: "MovieHeader", bundle: NSBundle.mainBundle()), forHeaderFooterViewReuseIdentifier: "Movie Header Cell")
        
        tableView.registerClass(MWMovieOverviewCell.self, forCellReuseIdentifier: movieOverviewCellReuseIdentifier)
        tableView.registerClass(MWMovieCrewCell.self, forCellReuseIdentifier: crewCellReuseIdentifier)
        
        tableView.allowsSelection = false
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 140.0
    }
    
    override func viewWillAppear(animated: Bool) {
        fetch()
    }

    override func viewDidAppear(animated: Bool)
    {
        super.viewDidAppear(animated)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "contentSizeCategoryChanged:", name: UIContentSizeCategoryDidChangeNotification, object: nil)
    }
    
    override func viewDidDisappear(animated: Bool)
    {
        super.viewDidDisappear(animated)
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIContentSizeCategoryDidChangeNotification, object: nil)
    }
    
    // This function will be called when the Dynamic Type user setting changes (from the system Settings app)
    func contentSizeCategoryChanged(notification: NSNotification)
    {
        tableView.reloadData()
    }
}

// MARK: - Table view data source
extension MWMovieDetailViewController: UITableViewDataSource {
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 2 {
            return 3
        } else {
            return 0
        }
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var header = UIView()
        switch section {
        case 0: // movie backdrop image
//            if let carouselHeader = tableView.dequeueReusableCellWithIdentifier("Movie Header") as? MWMovieHeaderCell {
//                
//            }
            if let path = self.backdropPath {
                let imageURL = NSURL(string: path)
                
                backdropImageView.sd_setImageWithURL(imageURL, placeholderImage: UIImage(named:"SeasonPosterPlaceholder"))
                header = backdropImageView
            }
            
        case 1: // movie poster and info
            if let section1Header = tableView.dequeueReusableCellWithIdentifier(movieHeaderCellReuseIdentifier) as? MWMovieHeaderCell {

                section1Header.posterImageView.image = self.posterImage

                section1Header.movieTitleLabel.text = tmdbMovieJson["title"].string
                if let minutes = tmdbMovieJson["runtime"].int {
                    section1Header.runningTimeLabel.text = "\(minutes / 60) hr \(minutes % 60) min"
                }
                
                // Collect the genres from the Json
                let genres = join(", ", self.tmdbMovieJson["genres"].arrayValue.map {$0["name"].stringValue})
                section1Header.genresLabel.text = genres
                
                let voteAvg = tmdbMovieJson["vote_average"].doubleValue
                section1Header.ratingsLabel.text = "\(voteAvg)"
                let voteCount = tmdbMovieJson["vote_count"].intValue
                section1Header.votesLabel.text = "(\(voteCount) votes)"
                
                // Get the primary certification from the Json array
                //let certification = certificationJson.arrayValue.reduce("") { $0 + ($1["iso_3166_1"].stringValue == "US" ? $1["certification"].stringValue : "") }
                let certification = certificationJson.arrayValue.filter { $0["primary"].boolValue }.first?["certification"].string
                
                section1Header.certificationLabel.text = certification ?? "--"
                
                if let c = certification {
                    switch c {
                    case "R":
                        section1Header.certificationImageView.image = UIImage(named: "RATED_R")
                    case "PG":
                        section1Header.certificationImageView.image = UIImage(named: "RATED_PG")
                    case "PG-13":
                        section1Header.certificationImageView.image = UIImage(named: "RATED_PG-13")
                    case "G":
                        section1Header.certificationImageView.image = UIImage(named: "RATED_G")
                    default: break
                    }
                }
                
                header = section1Header
            }

        case 2: // segmented control
            if let controlHeader = tableView.dequeueReusableCellWithIdentifier(segmentedControlCellReuseIdentifier) as? UITableViewCell {
                header = controlHeader
            }
            
        default:
            break
        }
        return header
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:     // Row #1 in section #2
            if let cell = tableView.dequeueReusableCellWithIdentifier(movieOverviewCellReuseIdentifier, forIndexPath: indexPath) as? MWMovieOverviewCell {
                cell.overviewLabel.text = self.tmdbMovieJson["overview"].string
                cell.tagLineLabel.text = self.tmdbMovieJson["tagline"].string ?? "Overview"
                
                // Make sure the constraints have been added to this cell, since it may have just been created from scratch
                cell.setNeedsUpdateConstraints()
                cell.updateConstraintsIfNeeded()
                
                return cell
            } else {
                assert(false, "The dequeued table view cell was of an unknown type!")
                return UITableViewCell()
            }

        case 1:     // Row #2 in section #2
            // This table row is setup in Storyboard
            let cell = tableView.dequeueReusableCellWithIdentifier(movieCastCellReuseIdentifier, forIndexPath: indexPath) as! MWMovieCastCell
            cell.setNeedsUpdateConstraints()
            cell.updateConstraintsIfNeeded()

            return cell
            
        case 2:     // Row #3 in section #2
            let cell = tableView.dequeueReusableCellWithIdentifier(crewCellReuseIdentifier, forIndexPath: indexPath) as! MWMovieCrewCell
            
            cell.titleLabel.text = "Crews"
            
            let productionDept: [JSON] = movieCrewJson.arrayValue.filter { $0["department"].string == "Production" }
            let allProducers = productionDept.filter { $0["job"].string!.hasSuffix("Producer") }
            let producers = allProducers.map { ["name": $0["name"].stringValue, "job": $0["job"].stringValue ] }
                
            let allDirectors = movieCrewJson.arrayValue.filter { $0["job"].string == "Director" }
            let directors = allDirectors.map { ["name": $0["name"].stringValue, "job": $0["job"].stringValue ] }
            
            let allWriters = movieCrewJson.arrayValue.filter { $0["department"].string == "Writing"  }
            let writers = allWriters.map { ["name": $0["name"].stringValue, "job": $0["job"].stringValue ] }
            
            cell.crews = directors + producers + writers
            
            cell.setupViews()
            
            cell.setNeedsUpdateConstraints()
            cell.updateConstraintsIfNeeded()

            return cell
            
        case 3:     // Row #4 in section #2
            let cell = tableView.dequeueReusableCellWithIdentifier("Movie Information", forIndexPath: indexPath) as! MWMovieInformationCell
            if let homepage = self.tmdbMovieJson["homepage"].string {
                cell.homepageLabel.text = homepage
            }
            cell.setNeedsUpdateConstraints()
            cell.updateConstraintsIfNeeded()

            return cell
            
        default:
            return UITableViewCell()
        }
    }
    
    // we need to do something special for the table row with collection view in it
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == 1 {
            let cell = cell as! MWMovieCastCell
            cell.setCollectionViewDataSourceDelegate(self, index: indexPath.row)
        }
    }
        
//    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
//        return 180
//    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            //            if let image = backdropImageView.image {
            //                println("Image height \(image.size.height)")
            //                return image.size.height
            //            } else {
            //                return 0
            //            }
            return tableView.bounds.width * 0.56
        case 1:
            return 138 + 16.0
        default:
            return 46
        }
    }
}

// MARK: - Collection View Data source and Delegate
extension MWMovieDetailViewController:UICollectionViewDataSource,UICollectionViewDelegate {
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movieCastJson.count ?? 0
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(castCollectionCellReuseIdentifier, forIndexPath: indexPath) as! MWPersonCollectionViewCell

        let cast = movieCastJson[indexPath.row]

        let baseUrl = NSUserDefaults.standardUserDefaults().stringForKey("image_base_url")!
        
        if let imagePath = cast["profile_path"].string {
            let posterSize = NSUserDefaults.standardUserDefaults().arrayForKey("poster_sizes")![1] as! String
            let posterPath = baseUrl + posterSize + imagePath
            cell.imageView.sd_setImageWithURL(NSURL(string: posterPath), placeholderImage: UIImage(named: "SeasonPosterPlaceholder"))
        } else {
            cell.imageView.image = UIImage(named: "SeasonPosterPlaceholder")
        }
        cell.nameLabel.text = cast["name"].string
        cell.jobLabel.text = cast["character"].string
        cell.cellIndex = CellIndex(section: 0, item: indexPath.row)
        // println("Cast \(indexPath.row) of \(movieCastJson.count): \(cell.nameLabel.text) as \(cell.jobLabel.text)")
        
        return cell
    }

    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let detailVC = segue.destinationViewController as? MWPeopleViewController {
            if let identifier = segue.identifier {
                switch identifier {
                case "Show Person":
                    let cell = sender as! MWPersonCollectionViewCell
                    if let cellIndex = cell.cellIndex {
                        detailVC.tmdbPersonId = movieCastJson[cellIndex.item]["id"].int
                        detailVC.posterImage = cell.imageView?.image
                    }
                default: break
                }
            }
        }
    }
}
