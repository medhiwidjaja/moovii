//
//  MWMovieCrewCell.swift
//  Moovii
//
//  Created by Medhi on 7/15/15.
//  Copyright (c) 2015 MedhiWidjaja. All rights reserved.
//

import UIKit

class MWMovieCrewCell: UITableViewCell {

    var crews: [[String:String]]?
    
    var didSetupConstraints = false
    var didSetupViews = false
    
    let kStandardHorizontalInsets: CGFloat = 8.0
    let kStandardVerticalInsets: CGFloat = 8.0
    let kWideVerticalInsets: CGFloat = 15.0
    let kWideHorizontalInsets: CGFloat = 15.0
    
    var titleLabel:  UILabel = UILabel.newAutoLayoutView()
    var dummyLabel:  UILabel = UILabel.newAutoLayoutView()
    
    var leftLabelArray = [UILabel]()
    var rightLabelArray = [UILabel]()
    
    func setupViews() {
        if !didSetupViews {
            setPropertiesForLabel(titleLabel, fontSize: 17.0, color: UIColor.whiteColor())
            
            if let listOfCrews = crews {
                for item in listOfCrews {
                    let jobLabel = UILabel.newAutoLayoutView()
                    jobLabel.text = item["job"]
                    setPropertiesForLabel(jobLabel, fontSize: 12.0, color: UIColor.darkGrayColor())
                    jobLabel.textAlignment = .Right
                    leftLabelArray.append(jobLabel)
                    
                    let nameLabel = UILabel.newAutoLayoutView()
                    nameLabel.text = item["name"]
                    setPropertiesForLabel(nameLabel, fontSize: 12.0, color: UIColor.lightGrayColor())
                    rightLabelArray.append(nameLabel)
                }
            }
            
            contentView.backgroundColor = UIColor(red: 0.1, green: 0.1, blue: 0.1, alpha: 1) // Lead
            self.backgroundColor = UIColor(red: 0.1, green: 0.1, blue: 0.1, alpha: 1) // Lead
            contentView.addSubview(dummyLabel)
        }
        didSetupViews = true
    }
    
    override func updateConstraints()
    {
        if !didSetupConstraints {
            UIView.autoSetPriority(1000) {
//                self.directorLabel.autoSetContentHuggingPriorityForAxis(.Horizontal)
//                self.directorLabel.autoSetContentCompressionResistancePriorityForAxis(.Horizontal)
            }
            UIView.autoSetPriority(250) {
                self.dummyLabel.autoSetContentHuggingPriorityForAxis(.Horizontal)
            }
            
            // set dummy width to a ratio of the contentView's width, so that its right edge will be at at certain point horizontally
            dummyLabel.autoMatchDimension(.Width, toDimension: .Width, ofView: contentView, withMultiplier: 0.40)
            dummyLabel.autoPinEdgeToSuperviewEdge(.Top, withInset: kStandardVerticalInsets)
            dummyLabel.autoPinEdgeToSuperviewEdge(.Leading, withInset: kStandardVerticalInsets)
            
            titleLabel.autoPinEdgeToSuperviewEdge(.Top, withInset: kStandardVerticalInsets)
            titleLabel.autoPinEdgeToSuperviewEdge(.Leading, withInset: kWideHorizontalInsets)
            titleLabel.autoPinEdgeToSuperviewEdge(.Trailing, withInset: kStandardHorizontalInsets)
            
            // the first name will be fixed to the title label
            rightLabelArray.first?.autoPinEdge(.Top, toEdge: .Bottom, ofView: titleLabel, withOffset: kStandardVerticalInsets)
            rightLabelArray.first?.autoPinEdge(.Left, toEdge: .Right, ofView: dummyLabel, withOffset: kWideHorizontalInsets)
            rightLabelArray.first?.autoPinEdgeToSuperviewEdge(.Trailing, withInset: kStandardHorizontalInsets)
            leftLabelArray.first?.autoPinEdge(.Top, toEdge: .Top, ofView: rightLabelArray.first)
            leftLabelArray.first?.autoPinEdge(.Right, toEdge: .Right, ofView: dummyLabel)
            leftLabelArray.first?.autoPinEdgeToSuperviewEdge(.Leading, withInset: kWideHorizontalInsets)
           
            if rightLabelArray.count > 1 {
                var prevLabel = rightLabelArray.first
                for i in 1...rightLabelArray.count-1 {
                    var label = rightLabelArray[i]
                    label.autoPinEdge(.Top, toEdge: .Bottom, ofView: prevLabel, withOffset: kStandardVerticalInsets)
                    label.autoPinEdge(.Left, toEdge: .Left, ofView: prevLabel)
                    label.autoPinEdgeToSuperviewEdge(.Trailing, withInset: kStandardHorizontalInsets)
                    prevLabel = label
                }
            }
            
            if leftLabelArray.count > 1 {
                var prevLabel = leftLabelArray.first
                for i in 1...leftLabelArray.count-1 {
                    var label = leftLabelArray[i]
                    label.autoPinEdge(.Top, toEdge: .Bottom, ofView: prevLabel, withOffset: kStandardVerticalInsets)
                    label.autoPinEdge(.Left, toEdge: .Left, ofView: prevLabel)
                    label.autoPinEdge(.Right, toEdge: .Right, ofView: dummyLabel)
                    prevLabel = label
                }
            }
            
            // Pin the last label to the bottom of superview
            UIView.autoSetPriority(750) {
                leftLabelArray.last?.autoPinEdgeToSuperviewEdge(.Bottom, withInset: kWideVerticalInsets)
            }
            didSetupConstraints = true
        }
        
        super.updateConstraints()
    }

    func setPropertiesForLabel(label: UILabel, fontSize: CGFloat, color: UIColor) {
        label.lineBreakMode = .ByTruncatingTail
        label.numberOfLines = 1
        label.textAlignment = .Left
        label.textColor     = color
        label.font = UIFont.systemFontOfSize(fontSize)
        contentView.addSubview(label)
    }

}
