//
//  PersonCollectionViewCell.swift
//  Moovii
//
//  Created by Medhi on 7/15/15.
//  Copyright (c) 2015 MedhiWidjaja. All rights reserved.
//

import UIKit

class MWPersonCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var jobLabel: UILabel!
    
    var cellIndex: CellIndex?
}
