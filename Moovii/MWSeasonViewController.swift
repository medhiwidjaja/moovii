//
//  MWSeasonViewController.swift
//  Moovii
//
//  Created by Medhi on 7/28/15.
//  Copyright (c) 2015 MedhiWidjaja. All rights reserved.
//

import UIKit

private let seasonDetailCellReuseIdentifier = "SeasonDetailTableCell"
private let seasonOverviewCellReuseIdentifier = "SeasonOverviewTableCell"
private let episodeListCellReuseIdentifier = "EpisodeListTableCell"

class MWSeasonViewController: UITableViewController {

    var tmdbTVShowId: Int?
    var tmdbTVSeasonId: Int?
    var seasonNo: Int?
    var tvShowName: String?
    var tvSeasonJson: JSON = JSON("")
    var episodes: Array<JSON> = Array()
    var episodesCount: Int?
    var posterPath: String?
    var posterImage: UIImage?    
    
    func fetch() {
        if let id = tmdbTVSeasonId {
            MWTmdbClient.sharedClient.get(kMWTmdbTVSeasonNumber, parameters: ["id": id.description, "season_number": seasonNo!.description]) {
                content, jsonData, error in
                if error != nil {
                    println("Error fetching : \(error)")
                }
                if let json = jsonData {
                    self.tvSeasonJson = json
                    let title = self.tvSeasonJson["name"].stringValue
                    self.posterPath = json["poster_path"].stringValue
                    if let episode = json["episodes"].array {
                        self.episodes = episode
                        self.episodesCount = episode.count
                        println("\(title) has \(episode.count) episodes")
                    }
                } else {
                    println("Json is absent")
                }
                self.tableView?.reloadData()
            }
        }
    }
    
    // MARK: - Table view delegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.registerClass(MWTVDetailCell.self, forCellReuseIdentifier: seasonDetailCellReuseIdentifier)
        tableView.registerClass(MWMovieOverviewCell.self, forCellReuseIdentifier: seasonOverviewCellReuseIdentifier)
        //tableView.registerClass(MWSeasonListCell.self, forCellReuseIdentifier: seasonListCellReuseIdentifier)
        tableView.allowsSelection = false
        
        // These are required for auto layout to determine the height of the tableview cells
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 85.0
    }
    
    override func viewWillAppear(animated: Bool) {
        fetch()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rows = 0
        if let count = episodesCount {
            rows = count + 2        // Detail + Overview + number of seasons
        }
        return rows
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let currentCell = {() -> UITableViewCell? in
            return tableView.cellForRowAtIndexPath(indexPath)
        }
        
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCellWithIdentifier(seasonDetailCellReuseIdentifier, forIndexPath: indexPath) as! MWTVDetailCell
            cell.posterImage.image = posterImage
            cell.titleLabel.text = self.tvSeasonJson["name"].string
            cell.subtext1Label.text = self.tvSeasonJson["name"].string
            
            let voteAverage = self.tvSeasonJson["vote_average"].doubleValue
            cell.voteAverageLabel.text = "\(voteAverage.description)"
            
            let votesCount = self.tvSeasonJson["vote_count"].intValue
            cell.voteCountLabel.text = "(\(votesCount.description) votes)"
            
            let networks = join(", ", self.tvSeasonJson["networks"].arrayValue.map {$0["name"].stringValue})
            cell.subtext2Label.text = networks
            
            cell.setNeedsUpdateConstraints()
            cell.updateConstraintsIfNeeded()
            return cell
            
        case 1:
            let cell = tableView.dequeueReusableCellWithIdentifier(seasonOverviewCellReuseIdentifier, forIndexPath: indexPath) as! MWSeasonOverviewCell
            cell.overviewLabel.text = tvSeasonJson["overview"].string
            cell.titleLabel.text = "Overview"
            
            // Make sure the constraints have been added to this cell, since it may have just been created from scratch
            cell.setNeedsUpdateConstraints()
            cell.updateConstraintsIfNeeded()
            return cell
            
        default:
            if episodes.count > 0 {
                let cell = tableView.dequeueReusableCellWithIdentifier(episodeListCellReuseIdentifier, forIndexPath: indexPath) as! MWSeasonListCell
                let season = episodes[indexPath.row - 2]  // The first two rows are Detail and Overview
                
                let baseUrl = NSUserDefaults.standardUserDefaults().stringForKey("image_base_url")!
                let posterSize = NSUserDefaults.standardUserDefaults().arrayForKey("poster_sizes")![1] as! String
                let posterPath = baseUrl + posterSize + season["poster_path"].stringValue
                
                // Download the image asynchronously
                cell.posterImage.sd_setImageWithURL(NSURL(string: posterPath), placeholderImage: UIImage(named:"SeasonPosterPlaceholder"))
                
                let seasonNo = season["season_number"].intValue
                let str = "Specials"
                cell.season.text = "Season \(seasonNo == 0 ? str : seasonNo.description)"
                let numberOfEpisodes = season["episode_count"].intValue
                cell.subtext.text = "\(numberOfEpisodes.description) episodes"
                
                cell.setNeedsUpdateConstraints()
                cell.updateConstraintsIfNeeded()
                return cell
            } else {
                return UITableViewCell()
            }
        }
        
    }
    
    //    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    //        switch indexPath.row {
    //            case 0...1: return 158
    //            default:    return 69+16
    //        }
    //    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    }
    */
}

