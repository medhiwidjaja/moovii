//
//  MWHomePosterCellCollectionViewCell.swift
//  Moovii
//
//  Created by Medhi on 7/12/15.
//  Copyright (c) 2015 MedhiWidjaja. All rights reserved.
//

import UIKit

class MWHomePosterCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var genres: UILabel!
}
