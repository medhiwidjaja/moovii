//
//  MWCarouselHeaderCell.swift
//  Moovii
//
//  Created by Medhi on 7/23/15.
//  Copyright (c) 2015 MedhiWidjaja. All rights reserved.
//

import UIKit

class MWCarouselHeaderCell: UITableViewCell {
    
    var didSetupConstraints = false
    var scrollView: UIScrollView!
    var images: [NSURL]? {
        didSet(newImages) {
            if let count = newImages?.count {
                for i in 0...count {
                    let origin = CGPoint(x: self.scrollView.frame.size.width, y: 0.0)
                    let size = self.scrollView.frame.size
                    let subview = UIView(frame: CGRect(origin: origin, size: size))
                    //subview.addSubview(newImages![i])
                    self.scrollView.addSubview(subview)
                }
            }
        }
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String!)
    {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        setupViews()
    }
    
    func setupViews() {
        scrollView = UIScrollView.newAutoLayoutView()
            
        contentView.addSubview(scrollView)
        
        contentView.backgroundColor = UIColor(red: 0.1, green: 0.1, blue: 0.1, alpha: 1) // Lead
        self.backgroundColor = UIColor(red: 0.1, green: 0.1, blue: 0.1, alpha: 1) // Lead
    }
    
    override func updateConstraints()
    {
        if !didSetupConstraints {
            //scrollView.autoPinEdgesToSuperviewEdgesWithInsets(0.0)
            didSetupConstraints = true
        }
        
        super.updateConstraints()
    }


}
