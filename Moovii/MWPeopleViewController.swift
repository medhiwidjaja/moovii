//
//  MWPeopleViewController.swift
//  Moovii
//
//  Created by Medhi on 7/25/15.
//  Copyright (c) 2015 MedhiWidjaja. All rights reserved.
//

import UIKit

private let personDetailTableCellReuseIdentifier = "PersonDetailTableCell"
private let personBiographyTableCellReuseIdentifier = "PersonBiographyTableCell"
private let personMovieTableCellReuseIdentifier = "MoviesTableCell"
private let movieCollectionCellReuseIdentifier = "MovieCollectionCell"

class MWPeopleViewController: UITableViewController {
    var personJson = JSON.nullJSON
    var moviesJson = JSON.nullJSON
    var tmdbPersonId: Int?
    var posterImage: UIImage?
    var biography: String?
    var birthday: String?
    var homepage: String?
    var name: String?
    var placeOfBirth: String?
    var profilePath: String?
    let baseUrl = NSUserDefaults.standardUserDefaults().stringForKey("image_base_url")!
    let posterSize = NSUserDefaults.standardUserDefaults().stringArrayForKey("poster_sizes")![1] as! String
    
    func fetch() {
        if let id = tmdbPersonId {
            MWTmdbClient.sharedClient.get(kMWTmdbPerson, parameters: ["id": id.description]) {
                content, jsonData, error in
                if error != nil {
                    println("Error fetching : \(error)")
                }
                if let json = jsonData {
                    self.personJson = json
                    self.name = self.personJson["name"].stringValue
                    self.profilePath = self.baseUrl + self.posterSize + json["profile_path"].stringValue
                } else {
                    println("Json is absent")
                }
                self.tableView?.reloadData()
            }
            MWTmdbClient.sharedClient.get(kMWTmdbPersonCredits, parameters: ["id": id.description]) {
                content, jsonData, error in
                if let json = jsonData {
                    self.moviesJson = json["cast"]
                    println("Got \(self.moviesJson.count) known-for movies for \(self.name)")
                    let name = self.moviesJson[0]["title"].string
                    println("First one is \(name)")
                }
                self.tableView?.reloadData()
            }
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        fetch()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.registerClass(MWPersonDetailTableCell.self, forCellReuseIdentifier: personDetailTableCellReuseIdentifier)
        tableView.registerClass(MWMovieOverviewCell.self, forCellReuseIdentifier: personBiographyTableCellReuseIdentifier)
        // tableView.registerClass(MWMoviesTableViewCell.self, forCellReuseIdentifier: personMovieTableCellReuseIdentifier)
        
        tableView.allowsSelection = false
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 140.0
        
        tableView.backgroundColor = UIColor(red: 0.1, green: 0.1, blue: 0.1, alpha: 1) // Lead
    }
    
    
    override func viewDidAppear(animated: Bool)
    {
        super.viewDidAppear(animated)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "contentSizeCategoryChanged:", name: UIContentSizeCategoryDidChangeNotification, object: nil)
    }
    
    override func viewDidDisappear(animated: Bool)
    {
        super.viewDidDisappear(animated)
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIContentSizeCategoryDidChangeNotification, object: nil)
    }
    
    // This function will be called when the Dynamic Type user setting changes (from the system Settings app)
    func contentSizeCategoryChanged(notification: NSNotification)
    {
        tableView.reloadData()
    }

}

// MARK: - Table view data source
extension MWPeopleViewController: UITableViewDelegate, UITableViewDataSource {
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            if let cell = tableView.dequeueReusableCellWithIdentifier(personDetailTableCellReuseIdentifier, forIndexPath: indexPath) as? MWPersonDetailTableCell {
                cell.nameLabel.text = personJson["name"].string
                cell.birthdayLabel.text = personJson["birthday"].string
                cell.deathDayLabel.text = personJson["deathday"].string
                cell.placeOfBirthLabel.text = personJson["place_of_birth"].string
                cell.homepageLabel.text = personJson["homepage"].string
                cell.posterImage.image = posterImage
                
                // Make sure the constraints have been added to this cell, since it may have just been created from scratch
                cell.setNeedsUpdateConstraints()
                cell.updateConstraintsIfNeeded()
                
                return cell
            } else {
                assert(false, "The dequeued table view cell was of an unknown type!")
                return UITableViewCell()
            }
            
        case 1:     // Row #1 in section #2
            if let cell = tableView.dequeueReusableCellWithIdentifier(personBiographyTableCellReuseIdentifier, forIndexPath: indexPath) as? MWMovieOverviewCell {
                cell.tagLineLabel.text = "Biography"
                cell.overviewLabel.text = self.personJson["biography"].string
                
                // Make sure the constraints have been added to this cell, since it may have just been created from scratch
                cell.setNeedsUpdateConstraints()
                cell.updateConstraintsIfNeeded()
                
                return cell
            } else {
                assert(false, "The dequeued table view cell was of an unknown type!")
                return UITableViewCell()
            }
            
        case 2:     // Row #2 in section #2
            let cell = tableView.dequeueReusableCellWithIdentifier(personMovieTableCellReuseIdentifier, forIndexPath: indexPath) as! MWMoviesTableViewCell            
            cell.headerLabel.text = "Known for"
            
            // Make sure the constraints have been added to this cell, since it may have just been created from scratch
            cell.setNeedsUpdateConstraints()
            cell.updateConstraintsIfNeeded()
            
            return cell
        
        default:
            return UITableViewCell()
        }

    }
    
    // we need to do something special for the table row with collection view in it
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == 2 {
            let cell = cell as! MWMoviesTableViewCell
            cell.setCollectionViewDataSourceDelegate(self, index: indexPath.row)
        }
    }    
}

// MARK: - Collection View Data source and Delegate
extension MWPeopleViewController:UICollectionViewDataSource,UICollectionViewDelegate {
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return moviesJson.count ?? 0
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(movieCollectionCellReuseIdentifier, forIndexPath: indexPath) as! MWMoviePosterCell
        
        let cast = moviesJson[indexPath.row]
        
        let baseUrl = NSUserDefaults.standardUserDefaults().stringForKey("image_base_url")!
        
        if let imagePath = cast["poster_path"].string {
            let posterSize = NSUserDefaults.standardUserDefaults().arrayForKey("poster_sizes")![1] as! String
            let posterPath = baseUrl + posterSize + imagePath
            cell.imageView.sd_setImageWithURL(NSURL(string: posterPath), placeholderImage: UIImage(named: "SeasonPosterPlaceholder"))
        } else {
            cell.imageView.image = UIImage(named: "SeasonPosterPlaceholder")
        }
        cell.titleLabel.text = cast["title"].string
        cell.subtitleLabel.text = cast["character"].string
        cell.cellIndex = CellIndex(section: 0, item: indexPath.row)
        // println("Cast \(indexPath.row) of \(movieCastJson.count): \(cell.nameLabel.text) as \(cell.jobLabel.text)")
        
        return cell
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let detailVC = segue.destinationViewController as? MWMovieDetailViewController {
            if let identifier = segue.identifier {
                switch identifier {
                case "ShowMovieFromPerson":
                    let cell = sender as! MWMoviePosterCell
                    if let cellIndex = cell.cellIndex {
                        detailVC.tmdbMovieId = moviesJson[cellIndex.item]["id"].int
                        detailVC.posterImage = cell.imageView?.image
                    }
                default: break
                }
            }
        }
    }
}

