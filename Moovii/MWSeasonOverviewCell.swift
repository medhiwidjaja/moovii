//
//  MWSeasonOverviewCell.swift
//  Moovii
//
//  Created by Medhi on 7/19/15.
//  Copyright (c) 2015 MedhiWidjaja. All rights reserved.
//

import UIKit

class MWSeasonOverviewCell: UITableViewCell {

//    @IBOutlet weak var overviewLabel: UILabel!

    var didSetupConstraints = false
    
    let kLabelHorizontalInsets: CGFloat = 15.0
    let kLabelVerticalInsets: CGFloat = 8.0
    
    var titleLabel: UILabel = UILabel.newAutoLayoutView()
    var overviewLabel: UILabel = UILabel.newAutoLayoutView()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String!)
    {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupViews()
    }
    
    required init(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        
        setupViews()
    }
    
    func setupViews()
    {
        titleLabel.lineBreakMode = .ByTruncatingTail
        titleLabel.numberOfLines = 2
        titleLabel.textAlignment = .Left
        titleLabel.textColor = UIColor.whiteColor()
        
        overviewLabel.lineBreakMode = .ByTruncatingTail
        overviewLabel.numberOfLines = 0
        overviewLabel.textAlignment = .Left
        overviewLabel.textColor = UIColor.lightGrayColor()
        overviewLabel.font = UIFont.systemFontOfSize(12.0)
        
        contentView.addSubview(titleLabel)
        contentView.addSubview(overviewLabel)
        
        contentView.backgroundColor = UIColor(red: 0.1, green: 0.1, blue: 0.1, alpha: 1) // Lead
        self.backgroundColor = UIColor(red: 0.1, green: 0.1, blue: 0.1, alpha: 1) // Lead
    }
    
    override func updateConstraints()
    {
        if !didSetupConstraints {
            UIView.autoSetPriority(1000) {
                self.titleLabel.autoSetContentCompressionResistancePriorityForAxis(.Vertical)
                self.overviewLabel.autoSetContentCompressionResistancePriorityForAxis(.Vertical)
            }
            
            titleLabel.autoPinEdgeToSuperviewEdge(.Top, withInset: kLabelVerticalInsets)
            titleLabel.autoPinEdgeToSuperviewEdge(.Leading, withInset: kLabelHorizontalInsets)
            titleLabel.autoPinEdgeToSuperviewEdge(.Trailing, withInset: kLabelHorizontalInsets)
            
            // This constraint is an inequality so that if the cell is slightly taller than actually required, extra space will go here
            overviewLabel.autoPinEdge(.Top, toEdge: .Bottom, ofView: titleLabel, withOffset: kLabelVerticalInsets, relation: .GreaterThanOrEqual)
            
            overviewLabel.autoPinEdgeToSuperviewEdge(.Leading, withInset: kLabelHorizontalInsets)
            overviewLabel.autoPinEdgeToSuperviewEdge(.Trailing, withInset: kLabelHorizontalInsets)
            overviewLabel.autoPinEdgeToSuperviewEdge(.Bottom, withInset: kLabelVerticalInsets)
            
            didSetupConstraints = true
        }
        
        super.updateConstraints()
    }

}
