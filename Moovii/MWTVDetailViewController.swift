//
//  MWTVDetailViewController.swift
//  Moovii
//
//  Created by Medhi on 7/12/15.
//  Copyright (c) 2015 MedhiWidjaja. All rights reserved.
//

import UIKit

private let showDetailCellReuseIdentifier = "ShowDetailTableCell"
private let showOverviewCellReuseIdentifier = "ShowOverviewTableCell"
private let seasonListCellReuseIdentifier = "SeasonListTableCell"

class MWTVDetailViewController: UITableViewController {
    
    var tmdbTVShowId: Int?
    var tvShow: JSON = JSON("")
    var showSeasons: Array<JSON> = Array()
    var seasonsCount: Int?
    var posterPath: String?
    var posterImage: UIImage?
    var imageDownloadQueue = NSOperationQueue()
    var tvShowTitle: String?
    
    func fetch() {
        if let id = tmdbTVShowId {
            MWTmdbClient.sharedClient.get(kMWTmdbTV, parameters: ["id": id.description]) {
                content, jsonData, error in
                if error != nil {
                    println("Error fetching : \(error)")
                }
                if let json = jsonData {
                    self.tvShow = json
                    self.tvShowTitle = self.tvShow["name"].stringValue
                    self.posterPath = json["poster_path"].stringValue
                    if let seasons = json["seasons"].array {
                        self.showSeasons = seasons
                        self.seasonsCount = seasons.count
                        println("\(self.tvShowTitle) has \(seasons.count) seasons")
                    }
                } else {
                    println("Json is absent")
                }
                self.tableView?.reloadData()
            }
        }
    }
    
    // MARK: - Table view delegate

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.registerClass(MWTVDetailCell.self, forCellReuseIdentifier: showDetailCellReuseIdentifier)
        tableView.registerClass(MWSeasonOverviewCell.self, forCellReuseIdentifier: showOverviewCellReuseIdentifier)
        //tableView.registerClass(MWSeasonListCell.self, forCellReuseIdentifier: seasonListCellReuseIdentifier)
        tableView.allowsSelection = false
        
        // These are required for auto layout to determine the height of the tableview cells
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 85.0
    }
    
    override func viewWillAppear(animated: Bool) {
        fetch()
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rows = 0
        if let count = seasonsCount {
            rows = count + 2        // Detail + Overview + number of seasons
        }
        return rows
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let currentCell = {() -> UITableViewCell? in
            return tableView.cellForRowAtIndexPath(indexPath)
        }
        
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCellWithIdentifier(showDetailCellReuseIdentifier, forIndexPath: indexPath) as! MWTVDetailCell
            cell.posterImage.image = posterImage
            cell.titleLabel.text = self.tvShow["name"].string
            
            let genres = join(", ", self.tvShow["genres"].arrayValue.map {$0["name"].stringValue})
            cell.subtext1Label.text = genres
            
            let voteAverage = self.tvShow["vote_average"].doubleValue
            cell.voteAverageLabel.text = "\(voteAverage.description)"
            
            let votesCount = self.tvShow["vote_count"].intValue
            cell.voteCountLabel.text = "(\(votesCount.description) votes)"
            
            let networks = join(", ", self.tvShow["networks"].arrayValue.map {$0["name"].stringValue})
            cell.subtext2Label.text = networks
            
            cell.setNeedsUpdateConstraints()
            cell.updateConstraintsIfNeeded()
            return cell
        
        case 1:
            let cell = tableView.dequeueReusableCellWithIdentifier(showOverviewCellReuseIdentifier, forIndexPath: indexPath) as! MWSeasonOverviewCell
            cell.overviewLabel.text = tvShow["overview"].string
            cell.titleLabel.text = "Overview"
            
            // Make sure the constraints have been added to this cell, since it may have just been created from scratch
            cell.setNeedsUpdateConstraints()
            cell.updateConstraintsIfNeeded()
            return cell
        
        default:
            if showSeasons.count > 0 {
                let cell = tableView.dequeueReusableCellWithIdentifier(seasonListCellReuseIdentifier, forIndexPath: indexPath) as! MWSeasonListCell
                let season = showSeasons[indexPath.row - 2]  // The first two rows are Detail and Overview

                let baseUrl = NSUserDefaults.standardUserDefaults().stringForKey("image_base_url")!
                let posterSize = NSUserDefaults.standardUserDefaults().arrayForKey("poster_sizes")![1] as! String
                let posterPath = baseUrl + posterSize + season["poster_path"].stringValue
                
                // Download the image asynchronously
                cell.posterImage.sd_setImageWithURL(NSURL(string: posterPath), placeholderImage: UIImage(named:"SeasonPosterPlaceholder"))
                
                let seasonNo = season["season_number"].intValue
                let str = "Specials"
                cell.season.text = "Season \(seasonNo == 0 ? str : seasonNo.description)"
                let numberOfEpisodes = season["episode_count"].intValue
                cell.subtext.text = "\(numberOfEpisodes.description) episodes"

                cell.setNeedsUpdateConstraints()
                cell.updateConstraintsIfNeeded()
                return cell
            } else {
                return UITableViewCell()
            }
        }

    }
    
//    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
//        switch indexPath.row {
//            case 0...1: return 158
//            default:    return 69+16
//        }
//    }
 
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let detailVC = segue.destinationViewController as? MWSeasonViewController {
            if let identifier = segue.identifier {
                switch identifier {
                case "ShowSeasonDetail":
                    let cell = sender as! MWSeasonListCell
                    detailVC.posterImage = posterImage
                    detailVC.tvShowName = tvShowTitle
                    detailVC.tmdbTVShowId = tmdbTVShowId
                    detailVC.seasonNo = tableView.indexPathForSelectedRow()?.row
                default: break
                }
            }
        }
    }

}

