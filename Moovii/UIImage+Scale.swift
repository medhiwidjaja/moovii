//
//  UIImage+Scale.swift
//  Moovii
//
//  Created by Medhi on 7/13/15.
//  Copyright (c) 2015 MedhiWidjaja. All rights reserved.
//

import UIKit

extension UIImage
{
    func scaleToSize(newSize:CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0);
        self.drawInRect(CGRectMake(0, 0, newSize.width, newSize.height))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }
}

extension UIImage
{
    var aspectRatio: CGFloat {
        return size.height != 0 ? size.width / size.height : 0
    }
}