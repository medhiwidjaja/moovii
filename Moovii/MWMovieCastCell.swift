//
//  MWMovieCastCell.swift
//  Moovii
//
//  Created by Medhi on 7/15/15.
//  Copyright (c) 2015 MedhiWidjaja. All rights reserved.
//

import UIKit

class MWMovieCastCell: UITableViewCell {

    @IBOutlet weak var castCollectionView: UICollectionView!

    func setCollectionViewDataSourceDelegate(delegate: protocol<UICollectionViewDelegate, UICollectionViewDataSource>, index: Int) {
        self.castCollectionView.dataSource = delegate
        self.castCollectionView.delegate = delegate
        self.castCollectionView.tag = index
        self.castCollectionView.reloadData()
    }
}
