//
//  MWPersonDetailTableCell.swift
//  Moovii
//
//  Created by Medhi on 7/26/15.
//  Copyright (c) 2015 MedhiWidjaja. All rights reserved.
//

import UIKit

class MWPersonDetailTableCell: UITableViewCell {

    var posterImage: UIImageView = UIImageView.newAutoLayoutView()
    var nameLabel: UILabel = UILabel.newAutoLayoutView()
    var birthdayLabel: UILabel = UILabel.newAutoLayoutView()
    var deathDayLabel: UILabel = UILabel.newAutoLayoutView()
    var placeOfBirthLabel: UILabel = UILabel.newAutoLayoutView()
    var homepageLabel: UILabel = UILabel.newAutoLayoutView()

    var didSetupConstraints = false
    
    let kLabelHorizontalInsets: CGFloat = 15.0
    let kLabelVerticalInsets: CGFloat = 8.0
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String!)
    {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupViews()
    }
    
    required init(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        
        setupViews()
    }
    
    func setupViews()
    {        
        nameLabel.lineBreakMode = .ByTruncatingTail
        nameLabel.numberOfLines = 2
        nameLabel.textAlignment = .Left
        nameLabel.textColor = UIColor.whiteColor()
        
        birthdayLabel.lineBreakMode = .ByTruncatingTail
        birthdayLabel.numberOfLines = 0
        birthdayLabel.textAlignment = .Left
        birthdayLabel.textColor = UIColor.lightGrayColor()
        birthdayLabel.font = UIFont.systemFontOfSize(12.0)
        
        deathDayLabel.lineBreakMode = .ByTruncatingTail
        deathDayLabel.numberOfLines = 0
        deathDayLabel.textAlignment = .Left
        deathDayLabel.textColor = UIColor.lightGrayColor()
        deathDayLabel.font = UIFont.systemFontOfSize(12.0)
        
        placeOfBirthLabel.lineBreakMode = .ByTruncatingTail
        placeOfBirthLabel.numberOfLines = 0
        placeOfBirthLabel.textAlignment = .Left
        placeOfBirthLabel.textColor = UIColor.lightGrayColor()
        placeOfBirthLabel.font = UIFont.systemFontOfSize(12.0)
        
        homepageLabel.lineBreakMode = .ByTruncatingTail
        homepageLabel.numberOfLines = 0
        homepageLabel.textAlignment = .Left
        homepageLabel.textColor = UIColor.lightGrayColor()
        homepageLabel.font = UIFont.systemFontOfSize(12.0)
        
        contentView.addSubview(posterImage)
        contentView.addSubview(nameLabel)
        contentView.addSubview(birthdayLabel)
        contentView.addSubview(deathDayLabel)
        contentView.addSubview(placeOfBirthLabel)
        contentView.addSubview(homepageLabel)
        
        contentView.backgroundColor = UIColor(red: 0.1, green: 0.1, blue: 0.1, alpha: 1) // Lead
        self.backgroundColor = UIColor(red: 0.1, green: 0.1, blue: 0.1, alpha: 1) // Lead
    }
    
    override func updateConstraints()
    {
        if !didSetupConstraints {
            
            posterImage.autoPinEdgeToSuperviewEdge(.Top, withInset: kLabelVerticalInsets)
            posterImage.autoPinEdgeToSuperviewEdge(.Leading, withInset: kLabelHorizontalInsets)
            
            // We set the height constraint of the image with a slightly lower priority to avoid
            // the annoying UIView-Encapsulated-Layout-Height auto layout issue
            // The height will be set properly by Auto Layout that will determine the height of the containing TableView cell
            UIView.autoSetPriority(800) {
                self.posterImage.autoSetDimension(.Height, toSize: 138.0)
            }
            
//            UIView.autoSetPriority(1000) {
//                self.birthdayLabel.autoSetContentHuggingPriorityForAxis(.Horizontal)
//                self.birthdayLabel.autoSetContentCompressionResistancePriorityForAxis(.Horizontal)
//            }
            
            posterImage.autoMatchDimension(.Width, toDimension: .Height, ofView: posterImage, withMultiplier: posterImage.image!.aspectRatio)
            posterImage.autoPinEdgeToSuperviewEdge(.Bottom, withInset: kLabelVerticalInsets, relation: .GreaterThanOrEqual)
            
            nameLabel.autoPinEdge(.Left, toEdge: .Right, ofView: posterImage, withOffset: kLabelHorizontalInsets)
            nameLabel.autoPinEdgeToSuperviewEdge(.Top, withInset: kLabelVerticalInsets)
            nameLabel.autoPinEdgeToSuperviewEdge(.Trailing, withInset: kLabelHorizontalInsets)
            
            birthdayLabel.autoPinEdge(.Top, toEdge: .Bottom, ofView: nameLabel, withOffset: kLabelVerticalInsets)
            birthdayLabel.autoPinEdge(.Left, toEdge: .Left, ofView: nameLabel)
            birthdayLabel.autoPinEdgeToSuperviewEdge(.Trailing, withInset: kLabelHorizontalInsets)
            
            deathDayLabel.autoPinEdge(.Top, toEdge: .Bottom, ofView: birthdayLabel, withOffset: kLabelVerticalInsets)
            deathDayLabel.autoPinEdge(.Left, toEdge: .Left, ofView: birthdayLabel)
            deathDayLabel.autoPinEdgeToSuperviewEdge(.Trailing, withInset: kLabelHorizontalInsets)
            
            placeOfBirthLabel.autoPinEdge(.Top, toEdge: .Bottom, ofView: deathDayLabel, withOffset: kLabelVerticalInsets)
            placeOfBirthLabel.autoPinEdge(.Left, toEdge: .Left, ofView: deathDayLabel)
            placeOfBirthLabel.autoPinEdgeToSuperviewEdge(.Trailing, withInset: kLabelHorizontalInsets)
            
            homepageLabel.autoPinEdge(.Top, toEdge: .Bottom, ofView: placeOfBirthLabel, withOffset: kLabelVerticalInsets)
            homepageLabel.autoPinEdge(.Left, toEdge: .Left, ofView: placeOfBirthLabel)
            homepageLabel.autoPinEdgeToSuperviewEdge(.Trailing, withInset: kLabelHorizontalInsets)
            
            didSetupConstraints = true
        }
        
        super.updateConstraints()
    }

}
