
//
//  MWMoviesViewController.swift
//  Moovii
//
//  Created by Medhi on 7/18/15.
//  Copyright (c) 2015 MedhiWidjaja. All rights reserved.
//

import UIKit

class MWMoviesViewController: UITableViewController {

    var moviesPopularJson    : [JSON]?
    var moviesNowPlayingJson : [JSON]?
    var moviesUpcomingJson   : [JSON]?
    
    func fetch() {

        MWTmdbClient.sharedClient.get(kMWTmdbMoviePopular, parameters: nil) {
            content, jsonData, error in
            if error != nil {
                println("Error fetching : \(error)")
            }
            if let json = jsonData {
                if let results = json["results"].array {
                    self.moviesPopularJson = results
                    println("Got \(results.count) popular movies")
                }
            } else {
                println("Json is absent")
            }
            self.tableView?.reloadData()
        }
        
        MWTmdbClient.sharedClient.get(kMWTmdbMovieNowPlaying, parameters: nil) {
            content, jsonData, error in
            if error != nil {
                println("Error fetching : \(error)")
            }
            if let json = jsonData {
                if let results = json["results"].array {
                    self.moviesNowPlayingJson = results
                    println("Got \(results.count) now playing movies")
                }
            } else {
                println("Json is absent")
            }
            self.tableView?.reloadData()
        }
        
        MWTmdbClient.sharedClient.get(kMWTmdbMovieUpcoming, parameters: nil) {
            content, jsonData, error in
            if error != nil {
                println("Error fetching : \(error)")
            }
            if let json = jsonData {
                if let results = json["results"].array {
                    self.moviesUpcomingJson = results
                    println("Got \(results.count) upcoming movies")
                }
            } else {
                println("Json is absent")
            }
            self.tableView?.reloadData()
        }

    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        fetch()
    }

}

// MARK: - Table view data source
extension MWMoviesViewController: UITableViewDelegate, UITableViewDataSource {

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("MoviesTableCell", forIndexPath: indexPath) as! MWMoviesTableViewCell
        return cell
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        let collectionCell = cell as! MWMoviesTableViewCell
        switch indexPath.row {
        case 0: collectionCell.headerLabel.text = "Popular"
        case 1: collectionCell.headerLabel.text = "Now Playing"
        case 2: collectionCell.headerLabel.text = "Upcoming"
        default: break
        }
        
        collectionCell.setCollectionViewDataSourceDelegate(self, index: indexPath.row)
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 233
    }
}

// MARK: - Collection View Data source and Delegate
extension MWMoviesViewController:UICollectionViewDataSource,UICollectionViewDelegate {
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return moviesPopularJson?.count ?? 0
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("MovieCollectionCell", forIndexPath: indexPath) as! MWMoviePosterCell
        
        switch collectionView.tag {
        case 0:
            if let jsonData = moviesPopularJson?[indexPath.row] {
                setupCell(cell, json: jsonData, cellIndex: CellIndex(section: collectionView.tag, item: indexPath.row))
            }
        case 1:
            if let jsonData = moviesNowPlayingJson?[indexPath.row] {
                setupCell(cell, json: jsonData, cellIndex: CellIndex(section: collectionView.tag, item: indexPath.row))
            }
        case 2:
            if let jsonData = moviesUpcomingJson?[indexPath.row] {
                setupCell(cell, json: jsonData, cellIndex: CellIndex(section: collectionView.tag, item: indexPath.row))
            }

        default: break
        }

        return cell
    }
    
    func setupCell(cell: MWMoviePosterCell, json: JSON, cellIndex: CellIndex) {
        let itemNo = cellIndex.item
        let baseUrl = NSUserDefaults.standardUserDefaults().stringForKey("image_base_url")!
        if let imagePath = json["poster_path"].string {
            let posterSize = NSUserDefaults.standardUserDefaults().arrayForKey("poster_sizes")![1] as! String
            let posterPath = baseUrl + posterSize + imagePath
            cell.imageView.sd_setImageWithURL(NSURL(string: posterPath), placeholderImage: UIImage(named: "SeasonPosterPlaceholder"))
        }
        cell.titleLabel.text = json["title"].string
        cell.subtitleLabel.text = json["release_date"].string
        cell.cellIndex = cellIndex
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let detailVC = segue.destinationViewController as? MWMovieDetailViewController {
            if let identifier = segue.identifier {
                switch identifier {
                case "ShowMovieDetail":
                    let cell = sender as! MWMoviePosterCell
                    if let cellIndex = cell.cellIndex {
                        switch cellIndex.section {
                        case 0:
                            detailVC.tmdbMovieId = moviesPopularJson?[cellIndex.item]["id"].int
                        case 1:
                            detailVC.tmdbMovieId = moviesNowPlayingJson?[cellIndex.item]["id"].int
                        case 2:
                            detailVC.tmdbMovieId = moviesUpcomingJson?[cellIndex.item]["id"].int
                        default:
                            break
                        }
                        detailVC.posterImage = cell.imageView?.image
                    }
                default: break
                }
            }
        }
    }
}
