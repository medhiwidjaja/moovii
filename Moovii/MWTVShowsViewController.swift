//
//  TmdbTVShowsViewController.swift
//  Moovii
//
//  Created by Medhi on 7/9/15.
//  Copyright (c) 2015 MedhiWidjaja. All rights reserved.
//

import UIKit
import WebImage

class MWTVShowsViewController: UITableViewController {
    
    var tvShowsPopularJson  : [JSON]?
    var tvShowsOnTheAirJson : [JSON]?
    var tvShowsAiringJson   : [JSON]?
    
    func fetch() {
        if tvShowsPopularJson == nil {
            MWTmdbClient.sharedClient.get(kMWTmdbTVPopular, parameters: nil) {
                content, jsonData, error in
                
                if error != nil {
                    println("Error fetching : \(error)")
                }
                if let json = jsonData {
                    if let results = json["results"].array {
                        self.tvShowsPopularJson = results
                        println("Got \(results.count) popular TV shows")
                    }
                } else {
                    println("Json is absent")
                }
                self.tableView?.reloadData()
            }
        }
        if tvShowsOnTheAirJson == nil {
            MWTmdbClient.sharedClient.get(kMWTmdbTVOnTheAir, parameters: nil) {
                content, jsonData, error in
                
                if error != nil {
                    println("Error fetching : \(error)")
                }
                if let json = jsonData {
                    if let results = json["results"].array {
                        self.tvShowsOnTheAirJson = results
                        println("Got \(results.count) on the air TV shows")
                    }
                } else {
                    println("Json is absent")
                }
                self.tableView?.reloadData()
            }
        }
        if tvShowsAiringJson == nil {
            MWTmdbClient.sharedClient.get(kMWTmdbTVAiringToday, parameters: nil) {
                content, jsonData, error in
                
                if error != nil {
                    println("Error fetching : \(error)")
                }
                if let json = jsonData {
                    if let results = json["results"].array {
                        self.tvShowsAiringJson = results
                        println("Got \(results.count) TV shows airing today")
                    }
                } else {
                    println("Json is absent")
                }
                self.tableView?.reloadData()
            }
        }
        
    }

    override func viewWillAppear(animated: Bool) {
        fetch()
    }
}

// MARK: - Table view data source
extension MWTVShowsViewController: UITableViewDelegate, UITableViewDataSource {
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("TVShowsTableCell", forIndexPath: indexPath) as! MWTVShowsTableViewCell
        return cell
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        let collectionCell = cell as! MWTVShowsTableViewCell
        switch indexPath.row {
        case 0: collectionCell.headerLabel.text = "Popular"
        case 1: collectionCell.headerLabel.text = "On The Air"
        case 2: collectionCell.headerLabel.text = "Airing Today"
        default: break
        }
        
        collectionCell.setCollectionViewDataSourceDelegate(self, index: indexPath.row)
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 233
    }
}

// MARK: - Collection View Data source and Delegate
extension MWTVShowsViewController:UICollectionViewDataSource,UICollectionViewDelegate {
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // Notes:
        // collectionView.tag is set in tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath)
        // in the call to collectionCell.setCollectionViewDataSourceDelegate(self, index: indexPath.row)
        
        switch collectionView.tag {
        case 0:
            return tvShowsPopularJson?.count ?? 0
        case 1:
            return tvShowsOnTheAirJson?.count ?? 0
        case 2:
            return tvShowsAiringJson?.count ?? 0
        default: return  0
        }
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("TVShowsCollectionCell", forIndexPath: indexPath) as! MWTVPosterCell
        
        switch collectionView.tag {
        case 0:
            if let jsonData = tvShowsPopularJson?[indexPath.row] {
                setupCell(cell, json: jsonData, cellIndex: CellIndex(section: collectionView.tag, item: indexPath.row))
            }
        case 1:
            if let jsonData = tvShowsOnTheAirJson?[indexPath.row] {
                setupCell(cell, json: jsonData, cellIndex: CellIndex(section: collectionView.tag, item: indexPath.row))
            }
        case 2:
            if let jsonData = tvShowsAiringJson?[indexPath.row] {
                setupCell(cell, json: jsonData, cellIndex: CellIndex(section: collectionView.tag, item: indexPath.row))
            }
        default: break
        }
        
        return cell
    }
    
    func setupCell(cell: MWTVPosterCell, json: JSON, cellIndex: CellIndex) {
        let itemNo = cellIndex.item
        let baseUrl = NSUserDefaults.standardUserDefaults().stringForKey("image_base_url")!
        if let imagePath = json["poster_path"].string {
            let posterSize = NSUserDefaults.standardUserDefaults().arrayForKey("poster_sizes")![1] as! String
            let posterPath = baseUrl + posterSize + imagePath
            cell.imageView.sd_setImageWithURL(NSURL(string: posterPath), placeholderImage: UIImage(named: "SeasonPosterPlaceholder"))
        }
        cell.titleLabel.text = json["name"].string
        cell.subtitleLabel.text = json["genres"][0]["name"].stringValue
        cell.cellIndex = cellIndex
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let detailVC = segue.destinationViewController as? MWTVDetailViewController {
            if let identifier = segue.identifier {
                switch identifier {
                case "ShowTVShowDetail":
                    let cell = sender as! MWTVPosterCell
                    if let cellIndex = cell.cellIndex {
                        switch cellIndex.section {
                        case 0:
                            detailVC.tmdbTVShowId = tvShowsPopularJson?[cellIndex.item]["id"].int
                        case 1:
                            detailVC.tmdbTVShowId = tvShowsOnTheAirJson?[cellIndex.item]["id"].int
                        case 2:
                            detailVC.tmdbTVShowId = tvShowsAiringJson?[cellIndex.item]["id"].int
                        default:
                            break
                        }
                        detailVC.posterImage = cell.imageView?.image
                    }
                default: break
                }
            }
        }
    }
}