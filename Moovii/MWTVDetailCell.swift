//
//  MWTVDetailCell.swift
//  Moovii
//
//  Created by Medhi on 7/12/15.
//  Copyright (c) 2015 MedhiWidjaja. All rights reserved.
//

import UIKit

class MWTVDetailCell: UITableViewCell {

    var posterImage: UIImageView = UIImageView.newAutoLayoutView()
    var titleLabel: UILabel = UILabel.newAutoLayoutView()
    var subtext1Label: UILabel = UILabel.newAutoLayoutView()
    var subtext2Label: UILabel = UILabel.newAutoLayoutView()
    var voteAverageLabel: UILabel = UILabel.newAutoLayoutView()
    var voteCountLabel: UILabel = UILabel.newAutoLayoutView()
    
    var didSetupConstraints = false
    
    let kLabelHorizontalInsets: CGFloat = 15.0
    let kLabelVerticalInsets: CGFloat = 8.0
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String!)
    {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupViews()
    }
    
    required init(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        
        setupViews()
    }
    
    func setupViews()
    {
        titleLabel.lineBreakMode = .ByTruncatingTail
        titleLabel.numberOfLines = 2
        titleLabel.textAlignment = .Left
        titleLabel.textColor = UIColor.whiteColor()
        
        subtext1Label.lineBreakMode = .ByTruncatingTail
        subtext1Label.numberOfLines = 0
        subtext1Label.textAlignment = .Left
        subtext1Label.textColor = UIColor.lightGrayColor()
        subtext1Label.font = UIFont.systemFontOfSize(12.0)
        
        subtext2Label.lineBreakMode = .ByTruncatingTail
        subtext2Label.numberOfLines = 0
        subtext2Label.textAlignment = .Left
        subtext2Label.textColor = UIColor.lightGrayColor()
        subtext2Label.font = UIFont.systemFontOfSize(12.0)
        
        voteAverageLabel.lineBreakMode = .ByTruncatingTail
        voteAverageLabel.numberOfLines = 0
        voteAverageLabel.textAlignment = .Left
        voteAverageLabel.textColor = UIColor.lightGrayColor()
        voteAverageLabel.font = UIFont.systemFontOfSize(12.0)
        
        voteCountLabel.lineBreakMode = .ByTruncatingTail
        voteCountLabel.numberOfLines = 0
        voteCountLabel.textAlignment = .Left
        voteCountLabel.textColor = UIColor.lightGrayColor()
        voteCountLabel.font = UIFont.systemFontOfSize(12.0)
        
        contentView.addSubview(posterImage)
        contentView.addSubview(titleLabel)
        contentView.addSubview(subtext1Label)
        contentView.addSubview(subtext2Label)
        contentView.addSubview(voteAverageLabel)
        contentView.addSubview(voteCountLabel)
        
        contentView.backgroundColor = UIColor(red: 0.1, green: 0.1, blue: 0.1, alpha: 1) // Lead
        self.backgroundColor = UIColor(red: 0.1, green: 0.1, blue: 0.1, alpha: 1) // Lead
    }
    
    override func updateConstraints()
    {
        if !didSetupConstraints {
//            UIView.autoSetPriority(1000) {
//                //self.posterImage.autoSetContentCompressionResistancePriorityForAxis(.Vertical)
//                self.posterImage.autoSetContentHuggingPriorityForAxis(.Vertical)
//                self.showTitle.autoSetContentCompressionResistancePriorityForAxis(.Vertical)
//                self.genresLabel.autoSetContentCompressionResistancePriorityForAxis(.Vertical)
//                self.networksLabel.autoSetContentCompressionResistancePriorityForAxis(.Vertical)
//                self.voteAverageLabel.autoSetContentCompressionResistancePriorityForAxis(.Horizontal)
//                self.voteCountLabel.autoSetContentCompressionResistancePriorityForAxis(.Horizontal)
//            }
//            UIView.autoSetPriority(200) {
//                self.posterImage.autoSetContentCompressionResistancePriorityForAxis(.Vertical)
//                
//            }
            
            posterImage.autoPinEdgeToSuperviewEdge(.Top, withInset: kLabelVerticalInsets)
            posterImage.autoPinEdgeToSuperviewEdge(.Leading, withInset: kLabelHorizontalInsets)
            
            // We set the height constraint of the image with a slightly lower priority to avoid
            // the annoying UIView-Encapsulated-Layout-Height auto layout issue
            // The height will be set properly by Auto Layout that will determine the height of the containing TableView cell
            UIView.autoSetPriority(800) {
                self.posterImage.autoSetDimension(.Height, toSize: 138.0)
            }
            
            posterImage.autoMatchDimension(.Width, toDimension: .Height, ofView: posterImage, withMultiplier: posterImage.image!.aspectRatio)
            posterImage.autoPinEdgeToSuperviewEdge(.Bottom, withInset: kLabelVerticalInsets, relation: .GreaterThanOrEqual)
            
            titleLabel.autoPinEdge(.Left, toEdge: .Right, ofView: posterImage, withOffset: kLabelHorizontalInsets)
            titleLabel.autoPinEdgeToSuperviewEdge(.Top, withInset: kLabelVerticalInsets)
            titleLabel.autoPinEdgeToSuperviewEdge(.Trailing, withInset: kLabelHorizontalInsets)
            
            subtext1Label.autoPinEdge(.Top, toEdge: .Bottom, ofView: titleLabel, withOffset: kLabelVerticalInsets, relation: .GreaterThanOrEqual)
            subtext1Label.autoPinEdge(.Left, toEdge: .Left, ofView: titleLabel)
            subtext1Label.autoPinEdgeToSuperviewEdge(.Trailing, withInset: kLabelHorizontalInsets)
            
            subtext2Label.autoPinEdge(.Top, toEdge: .Bottom, ofView: subtext1Label, withOffset: kLabelVerticalInsets, relation: .GreaterThanOrEqual)
            subtext2Label.autoPinEdge(.Left, toEdge: .Left, ofView: subtext1Label)
            subtext2Label.autoPinEdgeToSuperviewEdge(.Trailing, withInset: kLabelHorizontalInsets)
            
            voteAverageLabel.autoPinEdge(.Bottom, toEdge: .Bottom, ofView: posterImage)
            voteAverageLabel.autoPinEdge(.Left, toEdge: .Left, ofView: titleLabel)
            voteAverageLabel.autoPinEdgeToSuperviewEdge(.Trailing, withInset: kLabelHorizontalInsets)
            
            voteCountLabel.autoPinEdge(.Bottom, toEdge: .Bottom, ofView: posterImage)
            voteCountLabel.autoPinEdge(.Left, toEdge: .Right, ofView: posterImage, withOffset: 40.0)
            voteCountLabel.autoPinEdgeToSuperviewEdge(.Trailing, withInset: kLabelHorizontalInsets)
            
            didSetupConstraints = true
        }
        
        super.updateConstraints()
    }
}
